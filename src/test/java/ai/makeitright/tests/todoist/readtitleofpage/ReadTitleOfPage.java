package ai.makeitright.tests.todoist.readtitleofpage;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;


public class ReadTitleOfPage {

    private String URL;
    private WebDriver driver;

//    @BeforeClass
//    public static void beforeClass() {
//        System.setProperty("inputParameters.url","http://mir-todoist-test-app.s3-website-us-east-1.amazonaws.com/");
//    }

    @Before
    public void before() {
        URL = System.getProperty("inputParameters.url");

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--no-sandbox");
        options.addArguments("--disable-dev-shm-usage");
        options.addArguments("--headless");
        options.addArguments("--disable-gpu");
        options.addArguments("--single-process");
        options.addArguments("--use-gl=swiftshader");
        options.addArguments("--no-zygote");
        driver = new ChromeDriver(options);
        driver.manage().window().maximize();
    }

    
    @Test
    public void test() {
        System.out.println("******************************Environment Vars*****************************");
        Map<String, String> enviorntmentVars  = System.getenv();
        
        Set<Map.Entry<String, String>> entries = enviorntmentVars.entrySet();
        Iterator<Map.Entry<String, String>> environmentsIterator = entries.iterator();
        while (environmentsIterator.hasNext()) {
            Map.Entry<String, String> environment = environmentsIterator.next();
            System.out.println("Key: " + environment.getKey() + " Value: " + environment.getValue());
        }

        System.out.println("*******************************Finish env Vars*****************************");

        driver.get(URL);
        String pageTitle = driver.getTitle();
        JSONObject obj = new JSONObject();
        obj.put("titleofpage", pageTitle);
        System.setProperty("output", JSONValue.toJSONString(obj));
        driver.close();

    }



}
